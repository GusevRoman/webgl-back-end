var express = require('express'),
    videoRouter = express.Router(),
    mkdirp = require('mkdirp'),
		rmdirRecursiveSync = require('rmdir-recursive').sync,
		bodyParser = require('body-parser'),
		fs = require('fs'),
		spawn = require('child_process').spawn,
		resetIndex = require(__dirname + '/captureRouter').resetIndex;

videoRouter.get('/', function(req, res) {
	console.log('We got an encode request!');
	
	// Send response back
		res.status(200);
		res.contentType('application/json');
		res.end(JSON.stringify({'msg': 'Video encoding started'}));


	// Create all parameters for starting FFMPEG script
	var timestamp = new Date().getTime();
	var filename = 'movie-'+timestamp + '.mp4';
	var args = '-r 30 -i output/test/%04d.png output/'+filename;
	var startTime = new Date();

	// Start node child process with FFMPEG command and arguments
	var ffmpeg  = spawn('ffmpeg', args.split(' '));

	var stdout = '';
	var stderr = '';

	ffmpeg.stdout.on('data', function (data) {
		stdout+= data;
		console.log('stdout: ' + data);
	});

	ffmpeg.stderr.on('data', function (data) {
		stderr += data;;
		console.log('stderr: ' + data);
	});

	// Event when video encoding done
	ffmpeg.on('exit', function (code) {
		resetIndex();
		console.log('child process exited with code ' + code);
		
		// Create a return json object
		var ret = {
		   status: 'ok', 
		   filename: filename, 
		   code:code, stdout:stdout, 
		   stderr:stderr, 
		   time: (new Date().getTime()-startTime.getTime())
		};
		
		// Clear pictures directory = removing and then creating it
		var path = 'output/test';
		
		// Remove dir
		try {
		  rmdirRecursiveSync(path);
		  console.log(path + ' removed');
		} catch (err) {
		  console.log(path + ' cant removed with status ' + err);
		}
		// Create dir
		mkdirp(path, function (err) {
    	if (err) console.error(err);
    	else console.log(path + ' folder created!');
		});

		
		console.log(ret);
	});
});

module.exports = videoRouter;