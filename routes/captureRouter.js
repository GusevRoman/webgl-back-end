var express = require('express'),
    captureRouter = express.Router(),
		bodyParser = require('body-parser'),
		fs = require('fs'),
		path = require('path'),
		pictureIndex = 1;


var pad = function(str, max) {
		return str.length < max ? pad("0" + str, max) : str;
	};

captureRouter.post('/', function(req, res, next){
	
	var errorMessage = false,
			err,
			outputPath;

	// Check if we have got picture in post request
	if(!req.body.frame || !req.body.frame.length){
		errorMessage = 'Missing or invalid "frame" parameter';
	}
	else if (!req.body.path) {
		errorMessage = 'Missing or invalid "path" parameter';
	}
	if(errorMessage){
		err = new Error(errorMessage);
		err.status = 503;
		next(err);
	} else {
		
		// Generating path for files
		outputPath = path.join(__dirname, '/../output', req.body.path.replace(/\\|\.\.|\//g, path.sep));
		console.log('Saving files to path: ' + outputPath);

			// Create Buffer and put image data there
			var imageDataBuffer = new Buffer(req.body.frame.replace('data:image/png;base64,', ''), 'base64'),
				filePath = path.join(outputPath, pad((pictureIndex).toString(), 4)+'.png');
				// Increment index for naming files
				pictureIndex += 1;
			// Doing this synchronously ensures that when sorting the files by date, they appear in the correct order.
			fs.writeFileSync(filePath, imageDataBuffer, 'binary', function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log(filePath + ' was saved!');
				}
			});
		
		res.send('Capture success. Look in "'+ outputPath +'" for your output images.');
	}
});

module.exports = captureRouter;

// Export resetIndex function to be able to reset index to 1 from another module
module.exports.resetIndex = function() {
	pictureIndex = 1;
};