var express = require('express'),
    renderRouter = express.Router(),
    spawn = require('child_process').spawn;

renderRouter.get('/', function(req, res) {
	
	// Fire browser and open render.html file for start rendering
	spawn('open', ['http://localhost:3000/render.html']);
	res.status(200);
	res.send('Video encoding started');
})

module.exports = renderRouter;