var canvas = document.getElementById('canvas');
var engine = new BABYLON.Engine(canvas, true);
var screenShotFunction;
var createScene = function (){

  // Loading scene from blender
  BABYLON.SceneLoader.Load('', 'scene.babylon', engine, function (scene) {

    scene.executeWhenReady(function () {
      
      // Getting camera and light from scene
      var camera = scene.cameras[0];  
      var light = scene.lights[0];

      // Correcting camera direction after importing from blender
      camera.rotation.y = Math.PI;
      
      // Do some hardcore changes because Babylon do not coorrectly import camera and light properties
      camera.fov = 0.5;
      light.angle = 1;
      light.exponent = 1;

      // With this line you can always control camera by mouse
      camera.attachControl(canvas);

      /*************
      * ANIMATION *
      *************/

      // Creating animation manually
      var cameraAnimation;
      var lightAnimation;

      // Get exported animation keys
      var keys = exportedKeys;

      // Create 2 animations for camera and light
      var animationCamera = new BABYLON.Animation('myAnimation', 'position', 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
      var animationLight = new BABYLON.Animation('myAnimation', 'position', 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
      
      // Ease function for smooth transition between framekeys
      var easingFunction = new BABYLON.CubicEase();
      easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
      animationCamera.setEasingFunction(easingFunction);
      animationLight.setEasingFunction(easingFunction);

      // Add exported keys to animations
      animationCamera.setKeys(keys);
      animationLight.setKeys(keys);

      // Push animations to objects
      camera.animations.push(animationCamera);
      light.animations.push(animationLight);
    

      /****************
      * BUTTON EVENTS *
      ****************/

      $('#start').click(function() {
        if (cameraAnimation) {
          cameraAnimation._paused = false;
        } else {
          cameraAnimation = scene.beginAnimation(camera, 0, 2311, true);
          lightAnimation = scene.beginAnimation(light, 0, 2311, true);
        }
      });

      $('#pause').click(function() {
        cameraAnimation.pause();
        lightAnimation.pause();
      });

      $('#reset').click(function() {
        cameraAnimation = scene.beginAnimation(camera, 0, 2311, true);
        lightAnimation = scene.beginAnimation(light, 0, 2311, true);
      });

      $('#render').click(function() {
        if (cameraAnimation) {
          cameraAnimation.pause();
          lightAnimation.pause();
        }
        $.ajax({
          method: 'get',
          url: '/render',
          success: function (reply) {
            console.log(reply);
          },
          error: function (reply) {
            console.log(reply);
          }
        });
      });

      engine.runRenderLoop(function() {
        scene.render();
      });
    });
  });
  
  return scene;
};

// Resize canvas size if window resized
window.addEventListener('resize', function () {
  engine.resize();
});

var scene = createScene();

