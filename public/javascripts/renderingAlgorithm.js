var canvas = document.getElementById('canvas');
var engine = new BABYLON.Engine(canvas, true);
var screenShotFunction;
var screenshotCanvas;
var interval;


/*******************
* Babylon.js scene *
*******************/

var createScene = function (){

  // Loading scene from blender exported scene
  BABYLON.SceneLoader.Load('', 'scene.babylon', engine, function (scene) {

    scene.executeWhenReady(function () {
      
      // Getting camera and light from scene
      var camera = scene.cameras[0];  
      var light = scene.lights[0];

      // Correcting camera direction after importing from blender
      camera.rotation.y = Math.PI;
      
      // Do some hardcore changes because Babylon do not coorrectly import camera and light properties
      camera.fov = 0.5;
      light.angle = 1;
      light.exponent = 1;

      // With this line you can always control camera by mouse
      camera.attachControl(canvas);


      /*************
      * ANIMATION *
      *************/

      // Creating animation manually
      var cameraAnimation;
      var lightAnimation;

      // Get exported animation keys
      var keys = exportedKeys;

      // Create 2 animations for camera and light
      var animationCamera = new BABYLON.Animation('myAnimation', 'position', 2, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
      var animationLight = new BABYLON.Animation('myAnimation', 'position', 2, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
      
      // Ease function for smooth transition between framekeys
      var easingFunction = new BABYLON.CubicEase();
      easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
      animationCamera.setEasingFunction(easingFunction);
      animationLight.setEasingFunction(easingFunction);

      // Add exported keys to animations
      animationCamera.setKeys(keys);
      animationLight.setKeys(keys);

      // Push animations to objects
      camera.animations.push(animationCamera);
      light.animations.push(animationLight);
      
      // Start Animations
      // In CameraAnimation last function in parameters is function which will be fired after animation finishes
      cameraAnimation = scene.beginAnimation(camera, 0, 2311, false, 1, function() {
        clearInterval(interval);
        // ToVideo will close browser window and force server to start video encoding
        toVideo();
      });
      lightAnimation = scene.beginAnimation(light, 0, 2311, true);


      /*********************
      * ScreenCapture Loop *
      *********************/

      // Function which will start screenshot capturing process with particular size
      screenShotFunction = function capture() {
	      var size = {width: 1920, height: 1080};
	      createScreenshot(engine, camera, size);
      };
      
      // Start capturing screenshots every 0.5 seconds
      interval = setInterval(screenShotFunction, 1000 / 2);

      // Starts rendering loop
      engine.runRenderLoop(function() {
        scene.render();
      });
    });
  });
  
  return scene;
};

var scene = createScene();


/*********************
* ScreenShot Capture *
*********************/

var createScreenshot = function (engine, camera, size) {
  var width = size.width;
  var height = size.height;
  var scene = camera.getScene();
  var previousCamera = null;

  if (scene.activeCamera !== camera) {
      previousCamera = scene.activeCamera;
      scene.activeCamera = camera;
  }
  
  // Creates texture from particular camera position with all scene meshes
  var texture = new BABYLON.RenderTargetTexture('screenShot', size, engine.scenes[0]);
  texture.renderList = engine.scenes[0].meshes;

  texture.onAfterRender = function () {
    // Read the contents of the framebuffer
    var numberOfChannelsByLine = width * 4;
    var halfHeight = height / 2;

    // Reading datas from WebGL
    var data = engine.readPixels(0, 0, width, height);

    // Magic Unicorns works here, they are transfering pixels :)
    for (var i = 0; i < halfHeight; i++) {
      for (var j = 0; j < numberOfChannelsByLine; j++) {
        var currentCell = j + i * numberOfChannelsByLine;
        var targetLine = height - i - 1;
        var targetCell = j + targetLine * numberOfChannelsByLine;

        var temp = data[currentCell];
        data[currentCell] = data[targetCell];
        data[targetCell] = temp;
      }
    }

    // Create a 2D canvas to store the result
    if (!screenshotCanvas) {
      screenshotCanvas = document.getElementById('screenShotCanvas');
    }

    screenshotCanvas.width = width;
    screenshotCanvas.height = height;
    var context = screenshotCanvas.getContext('2d');

    // Copy the pixels to a 2D canvas
    var imageData = context.createImageData(width, height);
    imageData.data.set(data);
    context.putImageData(imageData, 0, 0);

    // Get real image data from canvas with toDataURL
    var dataToSend = screenshotCanvas.toDataURL('image/png');

    // Send this image to server
    $.ajax({
      method: 'post',
      url: '/capture',
      data: {
        frame: dataToSend,
        path: 'test'
      },
      success: function (reply) {
        console.log(reply);
      },
      error: function (reply) {
        console.log(reply);
      }
    });
  };

  texture.render(true);
  texture.dispose();

  if (previousCamera) {
    scene.activeCamera = previousCamera;
  }
};


/***********************
* Start video encoding *
***********************/

// Force server to start video encoding and close browser window
var toVideo = function() {
  $.ajax({
    method: 'get',
    url: '/video',
    success: function (reply) {
      console.log(reply);
      window.close();
    },
    error: function (reply) {
      console.log(intermediateElement.innerText);
    }
  });
}