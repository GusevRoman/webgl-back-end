var express = require('express'),
		app = express(),
		bodyParser = require('body-parser'),
		videoRouter = require(__dirname + '/routes/videoRouter'),
		captureRouter = require(__dirname + '/routes/captureRouter'),
		renderRouter = require(__dirname + '/routes/renderRouter');

app.use(bodyParser.urlencoded({limit: '500mb'}));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/pics'));


// Route for save image
app.use('/capture', captureRouter);

// Route for start video encoding process
app.use('/video', videoRouter);

// Route for start rendering (load browser and start capturing screenshots)
app.use('/render', renderRouter);

// Route for all other routes
app.use(function(req, res) {
	res.status(404);
	res.send('Not Found');
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
	console.log('Server up at http://localhost:' + server.address().port + '/');
});
